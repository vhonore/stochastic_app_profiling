#!/bin/bash

#### This script verifies if for each run of DRD dataset for files [40-75MB] we extracted correctly 7 tasks ####

echo START VERIFICATION SCRIPT
list_folders=$(ls jobs)

for e in $list_folders
do
	r=$(ls jobs/$e/ | wc -l)
	if ((r!=7))
	then
		echo ERROR IN RUN $e: only $r tasks instead of 7
	fi
	files=$(ls jobs/$e)
	for f in $files
	do
		size=$(wc -c jobs/$e/$f | cut -d' ' -f1)
		if ((size==0))
		then
			echo ***WARNING, EMPTY FILE inside folder of job $e***
		fi 
	done
done

echo END VERIFICATION SCRIPT
