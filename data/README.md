# Copyright (c) 2019-2020 Univ. Bordeaux #


This folder contains all the gathered data of running the 312 inputs with SLANT application presented [here](../application).


We detail here the inside of the two folders that contains all the data generated in this study.






## data_runs

In this folder, we present the makespan and memory footprint of each run of the application in accordance with inputs in folder [application](../application).


### makespan

We measure the walltime using "time" command on Linux, for submissions using SLURM scheduler on [Plafrim](https://www.plafrim.fr/).
Each submission has been performed on a Haswell platform composed of a server with two Intel Xeon E5-2680v3
processors (12 core @ 2,5 GHz).

The 312 inputs have been divided into 12 sets of 26 inputs (indexation of inputs per set is described in [application](../application)).

We provide a plot of the raw walltime of each run and the plot that generates it.
Each file "output_X.csv" contains the walltime of each of the 26 inputs in set X.
We also provide a file that summarize all the walltime for the overall 312 inputs ("makespan_merged.csv").


### memory

This folder contains the raw data of data monitoring of each run of the application (folder [data](data))
as so as the associated plot of memory footprint (folder [plots](plots)).

We monitor the memory by taking the field "used memory" of vmstat command. This process is done every 2sec
of the execution of the application. Script "monitoring.sh" shows the script that we used to monitor SLANT application.





## DRD_inputs

This folder contains the data used in Section II and III of the paper when we study the 88 runs of SLANT based on DRD dataset files of
size [40-75MB], presented in [application folder](../application).

This folder contains two subfolders.


### memory_footprint

This folder contains the subset of the memory footprints of folder [Memory](../data_runs/memory) that corresponds to the 88runs considered
here. 


### jobs

"Jobs" folders contains the decomposition into 7 tasks of each memory footprint in "memory-footprint".
This decomposition is performed by script "extract_jobs.sh". A script "verify.sh" allows to check that 7tasks have actually been
created for each input. To restart the generation, just do "make".

To make it more clear, we plotted the memory footprint of each task decomposition in the folder corresponding to each run.
