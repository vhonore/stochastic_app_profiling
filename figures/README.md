# Copyright (c) 2019-2020 Univ. Bordeaux #


# Figures and result of the paper


In this folder, we present the scripts and simulation code that generates the figures of Section II, III and IV of the paper.





## Section II (plots and scripts available in [SectionII folder](SectionII))

In this section, we study the application of SLANT through the 312 inputs listed in folder [application](../application).



### High Level Observations 

Figure 2 presents the walltime of each of the run of the application. Figure 2 is already available in folder [makespan](../data/data_runs/makespan)

In Figure 3, we study the run-to-run variability for 3 inputs generating a walltime in each of the two packs we observe in Figure [2](../data/data_runs/makespan)
The name of the studied inputs are in the plotting script of the figure ([plot_variability.R](SectionII/plot_variability.R)) The data of variability are stored in file [variability.csv](SectionII/variability.csv).

Figure 4 studies the correlation between application walltime and size of input. The file [correlation_input.csv](SectionII/correlation_input.csv)
contains the data of the walltime of the application for each input, the size of each input (in MB) and its type (1 for OASIS, 2 for DRD dataset).
We plot the figure II using the script [correlation_input.csv](SectionII/correlation_input_walltime.R).



### Task Level Observations

Figure 6 corresponds to the memory footprint of input with global index 17 and 26 (over the overall 312) available in [this folder](../data/data_runs/memory/plots).

For the rest of this section, we restrict to the study of the 88 inputs in DRD dataset having a size between 40MB and 75MB (profile more interesting for our study).
Figure 8 presents the non-cumulative walltime of tasks. This is generated by measuring the duration of each task for each input.
The data are reported in [this file](SectionII/makespan_tasks_notCumul.csv). Figure 8 is generated by script [plot_task_walltime.R](SectionII/plot_task_walltime.R).

Table I is obtained by measuring the Pearson correlation factor between each task, that is the data used to plot Figure 8.

Figure 9 plots the probability that tasks i is finised at time t (code in script [plot_cumulative.R](SectionII/plot_cumulative.R)). We plot the $`y_i`$ functions
presented in Section II.C) of the paper.
To do so, we compute the number of occurence of each task finishing at each time stamp. At the end, by normalizing over the total number of inputs, we obtain a cumulative of the termination time of each tasks.





## Section III (plots and scripts available in [SectionIII folder](SectionIII))

Figure 10 presents the same idea as Figure 8. However, we fit this time the execution time of each task with a normal distribution. The code to do this procedure
is [plot_interpolation_walltime.R](SectionIII/plot_interpolation_walltime.R).

Figure 11 is also similar as Figure 9. However, we plot this time the cumulative distribution of the termination of the 7 tasks where the execution time of each task
follows a Normal distribution. Figure 11 plots the $`f_i`$ functions presented in section III.b) of the paper.

With the task model of this section and given previous plots, we plot some memory quantities that can be obtained from that. 
By sampling a certain number of inputs in our dataset of runs (with replacement), we estimate the average memory needed for a run, as so as the maximum
memory need as a function of time. All details on these quantities are in Section III.b) of the paper.
The code to generate and plot (Figure 12a and 12b) these features is [plot_memory_quantities.R](SectionIII/plot_memory_quantities.R).





## Section IV (plots and scripts available in [SectionIV folder](SectionIV))

In this section, we propose a new scheduling algorithm based on application model, and we compare
it to 2 state-of-the-art approches.



### Experimental Evaluation

In this section, we propose to evaluate different scheduling algorithms presented in Section IV.A) of the paper.

We present briefly here the principle of the evaluation and as so as the input and code of the experiments.

Given a reservation strategy consisting of two reservations $`(R_1 , T_1 , C_1 ), (R_2 , T_2 , C_2 )`$ and
an application of walltime t, s.t. $`T_1 < t \leq T_2`$ we define:


1. Its total reservation time: $`(R_1 + T_1 + C_1 ) + (R_2 + (T_2 - T_1 ) + C_2)`$. That is: 
```math
R_1 + R_2 + T_2 + C_1 + C_2;
```
Results are in file [reserv_time.csv](SectionIV/reserv_time.csv) and the figure
is obtained using script [plot_experiments.R](SectionIV/plot_experiments.R).


2. Its system utilization, i.e. its walltime divided by its
reservation time. Results are in file [utilization.csv](SectionIV/utilization.csv) and the figure
is obtained using script [plot_experiments.R](SectionIV/plot_experiments.R).


3. In addition, if we define $`M_1`$ and $`M_2`$ the memory requested for the reservations, we can define the weighted
requested memory as:
```math
\frac{(R_1 + T_1 + C_1 ) \times M_1 + (R_2 + T_2 - T_1 + C_2 ) \times M_2}{R_1 + R_2 + T_2 + C_1 + C_2}
```
Intuitively this is the total memory used by the different
reservations normalized by time. Results are in file [mem_req.csv](SectionIV/mem_req.csv) and the figure
is obtained using script [plot_experiments.R](SectionIV/plot_experiments.R).

We present in Figure 13 of the paper the study of above criteria to
compare the different algorithms.
The execution of the application is performed on the Haswell
platform. The k inputs chosen for the modeling phase used
to derive the algorithms are picked uniformly at random with
replacement in the DRD set. The evaluation is performed on
the set of 88 inputs from DRD. All evaluations are repeated
10 times.

The results presented in Figure 13 by using "make" in the folder.
This starts the simulation code in "main.py" that implements the algorithms, probability distributions and 
discretization procedure in submodules.
We precise in preamble of the "main.py" the 10 sets of training inputs that have been sampled for each different number of inputs considered.
From it, we extracted mean and variance as so as max memory peak that compute a reservation sequence for each algorithm, for each of the 10 training
sets of each size of considered inputs (average of 10 results per size of inputs for each algorithm).

Then, we evaluate each of these sequences on the testing set composed of all of 88inputs. For example, for a size of inputs $`k=5`$,
we plot for each algorithm the average results of each metric of the 88inputs (1points) for each of the ten sequences of reservations.



### Evaluation of peak-aware algorithm

The next step would be to see how one
could deduce a new and improved algorithm by using the
task-level information. Specifically, looking at Figure 12a, the
natural intuition is to make a first reservation of length 25
min (guaranteed to finish before the memory intensive task
\#5), allowing it to be a cheaper solution memory-wise.
We study the new version of our proposed algorithm that incorporates this additional
reservation. In this solution, if task \#4 finishes before
these 25 minutes, we cannot start task \#5 since we do not
have enough memory available, hence we checkpoint the
output and waste the remaining time.

This is already implemented in "main.py" and Figure 14 presents the results of this experiments.
The data corresponds to the one with  algo=4 in [mem_req.csv](SectionIV/mem_req.csv) for 
the weighted average memory requested and in [reserv_time.csv](SectionIV/reserv_time.csv) for the reservation time.