import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rc
from scipy.stats import lognorm
from scipy.stats import gamma as gamma_d
rc('text', usetex=True)

from matplotlib.ticker import NullFormatter  # useful for `logit` scale
from matplotlib.ticker import ScalarFormatter
from matplotlib.ticker import FixedLocator


from copy import *
import numpy as np
from numpy import log as ln
from cmath import *
import scipy
import json
import time

import os
import pickle
from settings import *
import distri
from tools import *
from algorithms import *
import numpy
import ast



init_settings()


##################################################################################################################################
#################################################### DATA OF ALGORITHM INPUTS ####################################################
##################################################################################################################################







#### 10 times sampling with replacement, with 5 inputs ####
# vector of means of global normal distribution for the 10 sets of samples
means_5=[122.946667,121.70667,121.61333,113.48667,131.200000,119.68667,109.5733,111.68000,122.0533,116.63333]
# vector of std of global normal distribution for the 10 sets of samples
stds_5=[5.075573,11.65012,16.08233,10.50494,9.865146,15.95204,13.6320,10.74662,21.0550,17.00145]
### Algo 3: makespan max of over the 10 different sets 
max_runs_5=[7750,8032,8280,7602,8614,8334,7418,7596,8614,8614]



   
  
#### 10 times sampling with replacement, with 10 inputs ####
# vector of means of global normal distribution for the 10 sets of samples
means_10=[114.676667,115.506667,117.4100,117.31000,118.34000,126.61667,120.42667,122.0067,120.70000,119.37333]
# vector of std of global normal distribution for the 10 sets of samples
stds_10=[8.543918,7.680885,10.7098,10.17046,12.25212,15.50232,14.64915,15.9949,12.33138,15.47685]
### Algo 3: makespan max of over the 10 different sets 
max_runs_10=[7516,7792,8218,8294,8280,8614,9120,9120,8390,8536]





#### 10 times sampling with replacement, with 20 inputs ####
# vector of means of global normal distribution for the 10 sets of samples
means_20=[122.14000,120.09000,116.29333,118.87833,120.43500,117.29500,119.6033,120.20000,115.82333,119.80500]
# vector of std of global normal distribution for the 10 sets of samples
stds_20=[12.56298,13.09705,15.48446,14.01511,11.87005,12.97827,13.3366,14.40032,10.83044,11.69219]
### Algo 3: makespan max of over the 10 different sets
max_runs_20=[8390,8390,8550,9120,8614,8688,8688,8334,8090,8536]





#### 10 times sampling with replacement, with 50 inputs ####
# vector of means of global normal distribution for the 10 sets of samples
means_50=[119.97133,122.32667,115.45467,115.87933,119.13267,120.84533,119.75800,121.35667,123.04533,118.41000]
# vector of std of global normal distribution for the 10 sets of samples
stds_50=[13.33118,15.42153,12.71683,12.27876,14.10537,11.19775,14.89139,14.03402,13.42766, 13.48416]
### Algo 3: makespan max of over the 10 different sets
max_runs_50=[9120,9120,8614,8550,8688,8282,9120,9120,8614,8536]



  

#### 10 times sampling with replacement, with 100 inputs ####
# vector of means of global normal distribution for the 10 sets of samples
means_100=[118.77800,116.42500,118.43700,119.43700,117.41900,116.63767,121.93700,118.20867,118.64233,119.0977]
# vector of std of global normal distribution for the 10 sets of samples
stds_100=[12.67543,12.43126,13.22965,13.00372,13.78138,14.18525,13.94921,12.90986,13.17477,13.2076]
### Algo 3: makespan max of over the 10 different sets
max_runs_100=[9120,8390,8688,9120,9120,8614,9120,8614,9120,9120]


##### get walltime for each input 1->88 #####
walltime=[]
file=open("walltime.csv")
lines=file.readlines()
for line in lines:
	walltime.append(int(line.strip("\n")))
# print(walltime)
########################################
	





# in MB/sec
read_bw=(143.618513324+194.0062004823+269.1947836887+165.7829337458+458.3459802132+489.4437037935+538.9473684211+562.3686723973+559.3401395859+569.9382000038+583.2191319236+509.0939549237+511.3519607739)/13
write_bw=(68.7248322148+84.0722495895+112.4010371651+165.7829337458+175.7130758184+185.5669155658+194.7515088294+200.750085237+202.8571129729+206.756665402+207.0981470103+208.032786147+207.6787503888)/12
latency=1.3/60.0


def checkpoint(size):
	return (latency+((1.0*size/write_bw)/60))


def restart(size):
	return (latency+((1.0*size/read_bw)/60))




# in MB
worst_checkpoint=50324.136

# set C and R in 
settings.R=(worst_checkpoint/read_bw)/60.0
settings.C=(worst_checkpoint/write_bw)/60.0

nbchunks=1000

distrib="TruncatedNormal"

settings.alpha = 1.0
settings.gamma_ = 0.0
settings.beta = 0.0 

nb_tests=10

# time of the application, from 0 to 9121 sec
time=[e for e in range(0,9121,2)]



list_apps=[str(i) for i in range(1,89)]
#"2","15","23","69","81"]
algo_index=["1","2","3","4"]

nb_inputs=["5","10","20","50"]#,"100"]
inp=[int(nb_inputs[i]) for i in range(0,len(nb_inputs))]





#################### ALGO1 ####################
print("\n\nAlgo 1\n")
algo_1=[]

for size in inp:
	sequences=[]
	print("---------------- # inputs = "+str(size)+" ----------------\n\n")
	# get associated mean and stdev to generate distribution
	means = locals()["means_"+str(size)]
	stds = locals()["stds_"+str(size)]

	# process the ten repetitions for each number of inputs
	for i in range(0,len(means)):

		init_distributions(means[i],stds[i],0,190)

		distrib_discrete=discretization_time(distrib, nbchunks)

		r=dynamic_programming_discrete_allcheckpoint(distrib_discrete,"TruncatedNormal",settings.linear,nbchunks)

		sequences.append(r)
	
	### post_process to add C and R to the sequences
	post_process=[]
	for l in sequences:
		ll = deepcopy(l)
		ll.pop(0)
		cpt=0
		res=[]
		for (a,b) in ll:
			l=[0,0,0]
			if (cpt):
				l[0]=settings.R
			else:
				cpt=cpt+1		
			l[1]=a
			if (b):
				l[2]=settings.C
			res.append(l)
		#print(str(res)+"\n")
		post_process.append(res)
	algo_1.append(post_process)
##################### ALGO1 ####################





#################### ALGO2 ####################
def ckpt_duration(ckpt_cost):
	return ((ckpt_cost/write_bw)/60.0)
algo_2=[]
print("\n\nAlgo 2\n")

for size in inp:
	sequences=[]
	print("---------------- # inputs = "+str(size)+" ----------------\n\n")

	# get list of means and stds for the current number of inputs
	means = locals()["means_"+str(size)]
	stds = locals()["stds_"+str(size)]

	for test in range(1,nb_tests+1):	
		vect_checkpoints=[]
		# open list of checkpoint costs costs and read it
		file="ckpt_cost/"+str(size)+"inputs/trace_"+str(test)+".csv"
		trace=[]
		f = open(file, "r")
		for line in f:
			trace.append(int(line))
		f.close()
		
		# initilize the distribution with mean and std of current set
		init_distributions(means[test-1],stds[test-1],0,190)
		distrib_discrete=discretization_time(distrib, nbchunks)
		
		# compute the associate vector of checkpoint durations for each $v_i$
		for vi in distrib_discrete[0]:
			v = int(round(vi*60)/2)
			if (v>=4560):
				vect_checkpoints.append(ckpt_duration(trace[len(trace)-1]/1000))
			elif(v in time):
				vect_checkpoints.append(ckpt_duration(trace[v-1]/1000))
			else:
				vect_checkpoints.append(ckpt_duration(trace[v]/1000))

		r=dynamic_programming_discrete_allcheckpoint_dynamic(distrib_discrete,vect_checkpoints,"TruncatedNormal",settings.linear,nbchunks)
		sequences.append(r)
	
	### post_process to add C and R to the sequences
	post_process=[]
	for l in sequences:
		ll = deepcopy(l)
		ll.pop(0)
		cpt=0
		res=[]
		for (a,b) in ll:
			l=[0,0,0]
			if (cpt):
				l[0]=settings.R
			else:
				cpt=cpt+1		
			l[1]=a
			l[2]=b
			res.append(l)
		#print(str(res)+"\n")
		post_process.append(res)
	algo_2.append(post_process)
#################### ALGO2 ####################





##################### ALGO3 ####################
print("\n\n\nAlgo 3\n")
algo_3 = []

for size in inp:
	print("---------------- # inputs = "+str(size)+" ----------------\n\n")
	max_runs = locals()["max_runs_"+str(size)]
	res_inp=[]
	for max_r in max_runs:
		ll = []
		ll.append([0,(max_r/60),settings.C])
		ll.append([settings.R,(max_r/60)*1.5,settings.C])
		ll.append([settings.R,((max_r/60*1.5)*1.5),settings.C])
		#print(str(ll)+"\n")
		res_inp.append(ll)	
	algo_3.append(res_inp)
##################### ALGO3 ####################





#################### ALGO4 ####################
algo_4=[]
print("\n\nAlgo 4\n")

for size in inp:
	sequences=[]
	print("---------------- # inputs = "+str(size)+" ----------------\n\n")

	# get list of means and stds for the current number of inputs
	means = locals()["means_"+str(size)]
	stds = locals()["stds_"+str(size)]

	for test in range(1,nb_tests+1):	
		vect_checkpoints=[]
		# open list of checkpoint costs costs and read it
		file="ckpt_cost/"+str(size)+"inputs/trace_"+str(test)+".csv"
		trace=[]
		f = open(file, "r")
		for line in f:
			trace.append(int(line))
		f.close()
		
		# initilize the distribution with mean and std of current set
		init_distributions(means[test-1],stds[test-1],0,190)
		distrib_discrete=discretization_time(distrib, nbchunks)
		
		# compute the associate vector of checkpoint durations for each $v_i$
		for vi in distrib_discrete[0]:
			v = int(round(vi*60)/2)
			if (v>=4560):
				vect_checkpoints.append(ckpt_duration(trace[len(trace)-1]/1000))
			elif(v in time):
				vect_checkpoints.append(ckpt_duration(trace[v-1]/1000))
			else:
				vect_checkpoints.append(ckpt_duration(trace[v]/1000))

		r=dynamic_programming_discrete_allcheckpoint_dynamic(distrib_discrete,vect_checkpoints,"TruncatedNormal",settings.linear,nbchunks)
		r.pop(0)
		### 131 = (190/1000)/25
		ckpt_25m=vect_checkpoints[131]
		r.insert(0,[25,ckpt_25m])
		sequences.append(r)
	
	### post_process to add C and R to the sequences
	post_process=[]
	for l in sequences:
		ll = deepcopy(l)
		cpt=0
		res=[]
		for (a,b) in ll:
			l=[0,0,0]
			if (cpt):
				l[0]=settings.R
			else:
				cpt=cpt+1		
			l[1]=a
			l[2]=b
			res.append(l)
		#print(str(res)+"\n")
		post_process.append(res)
	algo_4.append(post_process)
#################### ALGO4 ####################

















######################################################### EVALUATION ########################################################


#### get total reservation time for a sequence seq=(R_i,t,C_i)_i generated by algorithm  algo and evaluated on input app 
def eval_total_reservation_time(seq,app,algo):
	

	assert(seq)


	seq_T=[]
	past=0.0
	for reserv in seq:
		seq_T.append([reserv[0],reserv[1]-past,reserv[2]])
		past=reserv[1]
		

	total_reservation_time=0.0
	overhead=0.0


	memory_trace="app_mem_profile/"+app+".csv"
	ckpt_trace=[]
	f=open(memory_trace)
	lines=f.readlines()
	for line in lines:
		ckpt_trace.append(int(line.strip("\n")))
	

	
	makespan=walltime[int(app)-1]


	progress=0.0
	last_ckpt_size=0.0
	avg_requested_mem=0.0



	for index in range(0,len(seq_T)):
		current_resa=0.0
		min_= progress
		max_=0.0


		reserv=seq_T[index]
		total_reservation_time+=restart(last_ckpt_size) + reserv[1] + overhead   #R_{ie} + T_i and eventual lost work that have to be reprocessed
		
		current_resa+=restart(last_ckpt_size) + reserv[1] + overhead 
		tmp=int(((progress + reserv[1] + overhead)*60)/2)
	
		if(tmp>=len(ckpt_trace)):
			current_resa=restart(last_ckpt_size) + reserv[1] + overhead 
			m_=(int((min_*60)/2))
			mm_=(int(makespan/2))
			subset=[]
			if(m_==mm_):
				subset=[ckpt_trace[m_]]
			else:
				assert(mm_>m_)
				subset=[ckpt_trace[i] for i in range(int((min_*60)/2),int(makespan/2))]
			max_peak_resa = max(subset)/1000
			if(algo==1 or algo==3):
				max_peak_resa=worst_checkpoint
			avg_requested_mem+=   ((max_peak_resa)*current_resa)
			# print(max_peak_resa)
			# if(algo==2):
			# 	exit(0)
			return [total_reservation_time,avg_requested_mem/total_reservation_time]
		
		
		ckpt_duration=checkpoint(ckpt_trace[tmp]/1000)

		if (ckpt_duration>reserv[2]):
			# will have to reprocess more reserv1 from last checkpoint
			overhead+=reserv[1]
			total_reservation_time+=reserv[2]
			max_=progress+current_resa
			current_resa+=reserv[2]
			
		else:
			total_reservation_time+=ckpt_duration
			last_ckpt_size=ckpt_trace[tmp]/1000
			progress+=reserv[1] + overhead
			max_=progress
			overhead=0.0

			current_resa+=ckpt_duration


		subset=[ckpt_trace[i] for i in range(int((min_*60)/2),int((max_*60)/2))]
		max_peak_resa = max(subset)/1000
		if(algo==1 or algo==3):
			max_peak_resa=worst_checkpoint
		avg_requested_mem+= ((max_peak_resa)*current_resa)
		









output_reservation_time=[]
output_utilization=[]
output_mem_requested=[]

i=0
for algo in algo_index:
	output_reservation_time.append([])
	output_utilization.append([])
	output_mem_requested.append([])
	j=0
	for inp in nb_inputs:
		output_reservation_time[i].append([])
		output_utilization[i].append([])
		output_mem_requested[i].append([])
		for _ in range(0,nb_tests):
			output_reservation_time[i][j].append([])
			output_utilization[i][j].append([])
			output_mem_requested[i][j].append([])
		j+=1
	i+=1





i=0
for algo in algo_index:
	j=0
	for inputs in nb_inputs:	
		k=0
		for test in range(0,nb_tests):
			for app in list_apps:
		
				assert(int(app)>0 and int(app)<=88)
				
				memory_trace="app_mem_profile/"+app+".csv"
				
				res=eval_total_reservation_time(locals()["algo_"+algo][j][test],app,int(algo))
				output_reservation_time[i][j][k].append(res[0])
				output_utilization[i][j][k].append((walltime[int(app)-1]/60)/res[0])
				output_mem_requested[i][j][k].append(res[1])
			k+=1
		j+=1
	i+=1










############################ prepare data for plots ############################


# for each algo: one list for each #of inputs,  a list containing the average reserv time for 10 seq over 50 test inputs
plot_reserv_time=[]
plot_utilization=[]
plot_mem_req=[]



for i in range(0,len(algo_index)):
	plot_reserv_time.append([])
	plot_utilization.append([])
	plot_mem_req.append([])
	for j in range(0,len(nb_inputs)):
		plot_reserv_time[i].append([])
		plot_utilization[i].append([])
		plot_mem_req[i].append([])




for i in range(0,len(algo_index)):
	for j in range(0,len(nb_inputs)):
		for k in range(0,nb_tests):
			plot_reserv_time[i][j].append(numpy.mean(output_reservation_time[i][j][k]))
			plot_utilization[i][j].append(numpy.mean(output_utilization[i][j][k]))
			plot_mem_req[i][j].append(numpy.mean(output_mem_requested[i][j][k]))



############################ End prepare data for plots ############################


print("\n--------------\n")
print(plot_reserv_time)
print("\n--------------\n")
print(plot_utilization)
print("\n--------------\n")
print(plot_mem_req)



#### export as csv

file_reserv_time= open("reserv_time.csv", "w")
file_utilization= open("utilization.csv", "w")
file_mem_req= open("mem_req.csv", "w")

file_reserv_time.write("algo,inputs,seq\n")
file_utilization.write("algo,inputs,seq\n")
file_mem_req.write("algo,inputs,seq\n")


for i in range(0,3):
	for j in range(0,len(nb_inputs)):
		for k in range(1,nb_tests+1):
			s=(str(algo_index[i])+","+str(nb_inputs[j])+","+str(plot_reserv_time[i][j][k-1])+"\n")
			file_reserv_time.write(s)
			s=(str(algo_index[i])+","+str(nb_inputs[j])+","+str(plot_utilization[i][j][k-1])+"\n")
			file_utilization.write(s)
			s=(str(algo_index[i])+","+str(nb_inputs[j])+","+str(plot_mem_req[i][j][k-1])+"\n")
			file_mem_req.write(s)

file_reserv_time.close()
file_utilization.close()
file_mem_req.close()




file_expe_2_mem=open("ckpt_25min_mem.csv",'w')
file_expe_2_time=open("ckpt_25min_res_time.csv",'w')

file_expe_2_mem.write("algo,inputs,seq\n")
file_expe_2_time.write("algo,inputs,seq\n")

for i in [0,3]:
	for j in range(0,len(nb_inputs)):
		for k in range(1,nb_tests+1):
			s=(str(algo_index[i])+","+str(nb_inputs[j])+","+str(plot_mem_req[i][j][k-1])+"\n")
			file_expe_2_mem.write(s)
			s=(str(algo_index[i])+","+str(nb_inputs[j])+","+str(plot_reserv_time[i][j][k-1])+"\n")
			file_expe_2_time.write(s)

file_expe_2_mem.close()
file_expe_2_time.close()