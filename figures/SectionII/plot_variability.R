library(ggplot2)
library(scales) 
library(grid)
library(plyr)
library(stringr)
library(MASS)
require(reshape2)

setwd(".")

##### OASIS input 1 = ev_sw1_mni.nii.gz  (coming from DRD dataset, but same behavior as OASIS set (size ~10MB))                       
##### OASIS input 2 = sub-OAS31167_ses-d4564_T1w.nii.gz
##### OASIS input 3 = sub-OAS30218_ses-d0092_run-01_T1w.nii.gz
##### DRD input 1 = sub-rid000005_task-raiders_acq-8ch340vol_run-05_bold.nii.gz
##### DRD input 2 = sub-rid000020_task-raiders_acq-8ch344vol_run-02_bold.nii.gz
##### DRD input 3 = sub-rid000043_task-raiders_acq-8ch326vol_run-04_bold.nii.gz


f<-"variability"
FILE <- read.table(file.path('.',paste(f,".csv",sep="")),header = TRUE,sep = ",",stringsAsFactors = FALSE)
colnames(FILE) <- c("input", "makespan","type")

name<-"figure3.jpg"
FILE$input <- as.factor(FILE$input)
FILE$type <- as.factor(FILE$type)
ggplot(FILE,aes(x=input,y=makespan/60,fill=type))+geom_boxplot(notch=FALSE) + 
  scale_fill_discrete(name="Dataset",labels=c("OASIS-3", "DRD"))+
  ylim(40,(max(FILE$makespan/60)*1.05)) + 
    ylab("Walltime (min)") + xlab("Dataset") +
  geom_dotplot(binaxis='y', stackdir='center', dotsize=1,binwidth = 0.25) +
  stat_summary(fun.y=mean, geom="point", shape=23, size=4) +
  scale_x_discrete(breaks=c("1","2","3","4","5","6"),
                   labels=c("OASIS1", "OASIS2", "OASIS3","DRD1", "DRD2", "DRD3"))
ggsave(file.path(name), width=5.43, height=2.54)
