# Copyright (c) 2019-2020 Univ. Bordeaux #



# About the application and the considered inputs in this work #

## Application

We use SLANT application, available for download [at this adress](https://github.com/vanderbiltscl/SLANTbrainSeg).


## Inputs

We use inputs coming from two different databases.
In total, we ran SLANT application over 312 different files. All files are downloadable on the website of each dataset.
In case of difficulty, please contact [valentin.honore@inria.fr](valentin.honore@inria.fr)

### OASIS-3 dataset

All the considered files of this dataset are available in the file "OASIS_files.csv".
We selected 194 different files over this really big dataset.
To download it, simply make a request of access to the database and dowload the corresponding entries
by selection T1w format for target images.


### Darthmount Raiders Dataset (DRD)

This files of this dataset is freely available here or here.
We listed in "DRD_files.csv" all the input files we used in our study.
Note that 29 files of this dataset were having a different size (<15MB and >100MB) in compared with others (40-75MB).
During our study, we noted that these files had an execution time close to OASIS-3 files. 
Hence, these files are referenced in "OASIS_files.csv" document for coherence with the study of the paper that restrict to
files of input 40-75MB of DRD dataset.

### Others

We keep the original input file contained in the docker image of the application. Having a size similar than files of OASIS dataset,
this file (test_volume.nii.gz) is referenced in "OASIS_files.csv".


## Running the application

We provide our batch script to run the application in the file [batch.sh](batch.sh).
Our target platform, Plafrim, uses the Slurm scheduler to manage the submissions.
This script can be adapted to fit any type of scheduler.
Moreover, we monitor the memory consumption of each run using the script [monitoring.sh](monitoring.sh).
This script makes a measurement of the current memory state using vmstat command every 2 seconds.